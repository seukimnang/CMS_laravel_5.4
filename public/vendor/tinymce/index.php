
<script src="jquery-3.1.0.min.js"></script>
<script src="tinymce.min.js"></script>
<textarea class="tinymce">
</textarea>

<script>
		tinymce.init({
			relative_urls:false,
			selector: ".tinymce",theme: "modern",skin : "lightgray",
			toolbar_items_size: "small",
			inline_styles : true,
			
			plugins: [
				 "responsivefilemanager advlist autolink link image lists charmap print preview hr anchor pagebreak",
				 "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
				 "table contextmenu directionality emoticons paste textcolor filemanager code fullscreen"
		   ],
		   toolbar1: "responsivefilemanager fontselect | fontsizeselect | undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
		   toolbar2: "| filemanager insertfile | link unlink anchor | image media | forecolor backcolor  | print preview code ",
			external_filemanager_path: "/tinymce/plugins/filemanager/",
			filemanager_title: "Responsive Filemanager" ,
			external_plugins: { "filemanager" : "/tinymce/plugins/filemanager/plugin.min.js"}
			
		 });
		

</script>