<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\User;
use Session;
use Auth;
class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $users = User::where('activate',1)->orderBy('created_at','ASC')->paginate(10);
    	return view('users.list',compact('users'));
    }

    public function Create()
    {   
    	return view('users.create');
    }

    public function Store(Request $request)
    {
        $this->validate($request,[
                'name'=>'required',
                'email'=>'required',
                'password'=>'required',
                'user_type'=>'required',
            ]);
    	$data = new User;
		$data->name = $request->input('name');
		$data->email = $request->input('email');
		$data->user_type = $request->input('user_type');
		$data->password = bcrypt($request->input('password'));        
		$data->save();
        Session::flash('message', 'Create Successfully !');
        return Redirect::to('users');
    }
    public function Edit($id)
    {
        $user = User::find($id);
        return view('users.edit', ['users' => $user]);

    }
    public function Update(Request $request , $id)
    {
        $this->validate($request,[
                'name'=>'required',
                'email'=>'required',
                'password'=>'required',
                'user_type'=>'required',
            ]);
        
        $data = User::find($id);
        $data->name = $request->input('name');
        $data->email = $request->input('email');
        $data->user_type = $request->input('user_type');
        $data->password = bcrypt($request->input('password'));
        $data->update();
         // redirect
        Session::flash('message', 'Update Successfully !');
        return Redirect::to('users');
    }
    public function Destroy($id)
    {
        //dd($id);
        $user = User::find($id);
        $user->update(['activate'=> 0]);
        //dd($user);
        Session::flash('message', 'Delete Successfully !');
        return Redirect::to('users');
    }
    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('login');
    }
}
