<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Category;
use App\User;
use Session;
class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
    	$categorys = Category::where('activate',1)->orderBy('order')->paginate(10);
    	return view('category.list',compact('categorys'));
 
    }

    public function Create()
    {
    	$categorys = DB::table('categories')
    				->select(array('id','category'))
    				->where('main_id',0)
    				->where('activate',1)->get();
    	//dd($categorys);
    	return view('category.create',['categorys'=>$categorys]);
    }

    public function Store(Request $request)
    {
    	$this->validate($request,[
    			'category' => 'required',
                'order'=>'required',
    			'description' => 'required',
    			]);
    	$data = new Category;
    	$data->category = $request->input('category');
        $data->order = $request->input('order');
    	$data->description = $request->input('description');
    	$data->main_id = $request->input('main_id');
    	$data->save();

        Session::flash('message', 'Create Successfully !');
    	return Redirect::to('/category/create');
    }

    public function edit($id)
    {
    	$categorys = Category::find($id);
    	$category = DB::table('categories')
    				->select(array('id','category'))
    				->where('main_id',0)
    				->where('activate',1)
    				->get();
    	//dd($categorys);
        return view('category.edit', compact('categorys','category'));
    }

    public function Update(Request $request, $id)
    {
        $this->validate($request,[
                'category' => 'required',
                'order'=>'required',
                'description' => 'required',
                ]);
        $data = Category::find($id);
        $data->category = $request->input('category');
        $data->order = $request->input('order');
        $data->description = $request->input('description');
        $data->main_id = $request->input('main_id');
        $data->update();

        Session::flash('message', 'Update Successfully !');
        return Redirect::to('category');
    }
    public function Destroy($id)
    {
        //dd($id);
        $category = Category::find($id);
        $category->update(['activate'=> 0]);
        //dd($user);
        Session::flash('message', 'Delete Successfully !');
        return Redirect::to('category');
    }
}
