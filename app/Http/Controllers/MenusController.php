<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Menu;
use Session;
class MenusController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	$menu = Menu::where('activate',1)->paginate(10);
    	return view('menu.list',compact('menu'));
    }
    public function Create()
    {   
    	return view('menu.create',compact('category','page'));
    }

    public function Store(Request $request)
    {
    	$this->validate($request,[
    			'menu_name'=>'required',
    			]);
    	$data = new Menu;
		$data->menu_name = $request->input('menu_name');
		$data->save();

        Session::flash('message', 'Create Successfully !');
        return Redirect::to('menu');
    }
    public function Edit($id)
    {
        $menu = Menu::find($id);
        return view('menu.edit',compact('category','page','menu'));

    }
    public function Update(Request $request , $id)
    {
        $this->validate($request,[
                'menu_name'=>'required',
                ]);
        $data = Menu::find($id);
        $data->menu_name = $request->input('menu_name');
        $data->update();
         // redirect
        Session::flash('message', 'Update Successfully !');
        return Redirect::to('menu');
    }
    public function Destroy($id)
    {
        //dd($id);
        $menu = Menu::find($id);
        $menu->update(['activate'=> 0]);
        //dd($user);
        Session::flash('message', 'Delete Successfully !');
        return Redirect::to('menu');
    }
}
