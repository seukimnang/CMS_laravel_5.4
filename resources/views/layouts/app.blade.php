<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Dashboard</title>
    <link href="{{ asset('css/app.css')}}" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top" style="background-color: #428bca;">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#" style="color: white;">CMS_laravel</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">            
            <li><a href="#" style="color: white;"><i class="fa fa-user"> Profile</i></a></li>
            <li><a href="#" style="color: white;"><i class="fa fa-cogs"> Settings</i></a></li>
            <li><a href="{{ ('/logout/') }}" style="color: white;"><i class="fa fa-sign-out"> Logout</i></a></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid" >
      <div class="row" >
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="#"><i class="fa fa-home"> Dashboard </i> </a></li>
            <li><a href="{{('/users')}}"><i class="fa fa-user-circle"> USERS </i></a></li>
            <li><a href="{{'/post/'}}"><i class="fa fa-paper-plane-o"> POST </i></a></li>
            <li><a href="{{'/category/'}}"><i class="fa fa-cart-arrow-down"> CATEGORIES </i></a></li>
            <li><a href="{{('/page/')}}"><i class="fa fa-file-text-o"> PAGES </i></a></li>
            <li><a href="{{'/menu/'}}"><i class="fa fa-bars"> MENU </i></a></li>
            <li><a href="{{'/submenu/'}}"><i class="fa fa-bars">SUB MENU </i></a></li>
            <li><a href="{{('/tags/')}}"><i class="fa fa-id-card"> TAG </i></a></li>
            <li><a href="{{('/silders/')}}"><i class="fa fa-sliders"> SILDER </i></a></li>
          </ul>
        </div>
       
        
     