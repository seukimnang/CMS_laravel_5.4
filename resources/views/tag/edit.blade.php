@extends('layouts.app')
<link href="{{ asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h3 class="page-header" style="color: #428bca;"> 
        Update Tag
    </h3>
<form action="{{('/tags/update/'.$tags->id)}}" method="POST" class="form-horizontal"  >
                        {{ csrf_field() }}
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Update Tag</h3>
    </div>
        <div class="panel-body">
        	<div class="rows">
        		<div class="col-xs-6 {{ $errors->has('tag_name') ? 'has-error' : '' }}">   
					<label>
						Tag Name
						<span 
			              	class="text-danger">{{ $errors->first('tag_name') }}
			            </span>
					</label>
					<input 
					type="text"
					name="tag_name" 
					value="{{$tags->tag_name}}" 
					class="form-control" 
					placeholder="TAG NAME"
					>
        		</div>
        	</div>
        </div>
    <div class="panel-footer clearfix">
        <div class="pull-right">
			<input 
				type="submit" 
				value="Update" 
				class="btn btn-primary"
			>
			<a 
				href="{{'/tags'}}" 
				class="btn btn-primary"
			>
				CANCEL
			</a>
		</div>
	</div>
</div>
</form>