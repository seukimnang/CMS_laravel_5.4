@extends('layouts.app')
<link href="{{ asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h3 class="page-header" style="color: #428bca;"> 
        Create User
    </h3>
<form class="form-horizontal" method="POST" action="{{ ('/users') }}">
                        {{ csrf_field() }}
<div class="panel panel-default">

    <div class="panel-heading">
        <h3 class="panel-title">Create User</h3>
    </div>
        <div class="panel-body">
        	<div class="rows">
        		<div class="col-xs-6 {{ $errors->has('name') ? 'has-error' : '' }}">				   
					<label>User Name
						<span 
							class="text-danger">{{ $errors->first('name') }}
						</span>
					</label>
					<input 
					type="text" 
					name="name" 
					class="form-control" 
					placeholder="USER NAME">
					
					
					{{ $errors->has('email') ? 'has-error' : '' }}
					<label>Email 
						<span 
							class="text-danger">{{ $errors->first('email') }}
						</span>
					</label>
					<input 
					type="email" 
					name="email" 
					class="form-control" 
					placeholder="EMAIL"
					>

        		</div>
        	</div>
        	<div class="rows">
        		<div class="col-xs-6 {{ $errors->has('password') ? 'has-error' : '' }}">   
					<label>Password 
						<span 
							class="text-danger">{{ $errors->first('password') }}
						</span>
					</label>
					<input 
					type="password" 
					name="password" 
					class="form-control" 
					placeholder="PASSWORD"
					>
        		</div>
        	</div>
        	<div class="rows">
        		<div class="col-xs-6 {{ $errors->has('user_type') ? 'has-error' : '' }}">	
        		<label>Role 
        			<span 
						class="text-danger">{{ $errors->first('user_type') }}
					</span>
        		</label>
    		   	
					<select class="form-control" name="user_type">
						<option value="">Select Role</option>
						<option value="admin">admin</option>
						<option value="editor">Editor</option>
						<option value="author">Author</option>
						<option value="subscrib">subscrib</option>
					</select>
	
        		</div>
        	</div>
        </div>
    <div class="panel-footer clearfix">
        <div class="pull-right">
			<input 
				type="submit" 
				value="CREATE" 
				class="btn btn-primary"
			>
			<a 
				href="{{'/users'}}" 
				class="btn btn-primary"
			>
				CANCEL
			</a>
		</div>
	</div>
</div>
</form>