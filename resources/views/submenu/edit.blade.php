@extends('layouts.app')
<link href="{{ asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h3 class="page-header" style="color: #428bca;"> 
        Update Menu
    </h3>
<form action="{{ ('/menu/update/'.$menu->id) }}" class="form-horizontal" method="POST" >
                        {{ csrf_field() }}
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Update Menu</h3>
    </div>
        <div class="panel-body">
        	<div class="rows">
        		<div class="col-xs-6 {{ $errors->has('menu_name') ? 'has-error' : '' }}">				   
					<label>
						Menu Name
						<span 
			              class="text-danger">{{ $errors->first('menu_name') }}
			            </span>
					</label>
					<input 
					type="text" 
					name="menu_name"
					value="{{$menu->menu_name}}" 
					class="form-control" 
					placeholder="MENU NAME"
					>


        		</div>
        	</div>
        </div>
    <div class="panel-footer clearfix">
        <div class="pull-right">
			<input 
				type="submit" 
				value="UPDATE" 
				class="btn btn-primary"
			>
			<a 
				href="{{'/menu'}}" 
				class="btn btn-primary"
			>
				CANCEL
			</a>
		</div>
	</div>
</div>
</form>