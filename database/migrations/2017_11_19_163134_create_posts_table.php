<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->longText('description');
            $table->mediumText('sort_text');
            $table->integer('author')->unsigned();
            $table->string('status');
            $table->boolean('activate')->default(true);
            $table->dateTime('public_date');
            $table->dateTime('post_date');
            $table->dateTime('update_date');
            $table->integer('update_by');
            $table->dateTime('delete_date');
            $table->integer('delete_by');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references
                            ('id')->on('categories');
            $table->foreign('author')->references
                            ('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
