<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Silder;
use Session;
use Image;
class SildersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	$silder = Silder::where('activate',1)->get();
    	return view('silder.list',compact('silder'));
    }
    public function Create()
    {   
    	return view('silder.create');
    }

    public function Store(Request $request)
    {
    	$this->validate($request, 
                        [
    					'title'=>'required',
    					'sort_text'=>'required',
    					'description'=>'required',
    					'image'=>'required|image'
    					]);

    	$data = new Silder;
		$data->title = $request->input('title');   
		$data->sort_text = $request->input('sort_text'); 
		$data->description = $request->input('description');  
		$data->image = $request->file('image'); 
        $images = $request->file('image');
        $filename = time().'-'.str_slug($data['image'],"-").'.'.
                    $images->getClientOriginalExtension();
        $image_path = public_path('photo/upload/'.$filename);
        dd($image_path);

        $imageupload = Image::make($images->getRealPath())->save($image_path);
        dd($imageupload);

		//$data->save();

        //Session::flash('message', 'Create Successfully !');
        //return Redirect::to('silders');
    }
    public function Edit($id)
    {
        $tag = Silder::find($id);
        return view('tag.edit', ['tags' => $tag]);

    }
    public function Update(Request $request , $id)
    {
        
        $data = Silder::find($id);
        $data->tag_name = $request->input('tag_name');     
        $data->update();
         // redirect
        Session::flash('message', 'Update Successfully !');
        return Redirect::to('silders');
    }
    public function Destroy($id)
    {

        $silder = Silder::find($id);
        $silder->update(['activate'=> 0]);
        Session::flash('message', 'Delete Successfully !');
        return Redirect::to('silders');
    }
}
