@extends('layouts.app')
<script src="{{ asset('js/jquery.js')}}"></script>
<link href="{{ asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
<script type="text/javascript" src="{{ url('') }}/vendor/tinymce/tinymce.min.js"></script>

<script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    height : "200" , 
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
   $(function() {
    $( "#datepicker" ).datepicker({
        dateFormat: "yy-mm-dd ",

    });
  });
</script>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
  <h3 
    class="page-header" 
    style="color: #428bca;"> 
      Create Post
  </h3>
<form 
    class="form-horizontal" 
    method="POST" 
    action="{{ ('/post/store') }}">
    {{ csrf_field() }}
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Create Post</h3>
    </div>
    <div class="panel-body">
      <div class="rows">
        <input 
            type="hidden" 
            name="id" 
          >
        <div class="col-xs-6 {{ $errors->has('title') ? 'has-error' : '' }}">
    		    <label>Ttile
                <span 
                  class="text-danger">{{ $errors->first('title') }}
                </span>
            </label>
    			<input 
    				type="text" 
    				name="title" 
    				class="form-control" 
    				placeholder="TITLE"
    			>
            {{ $errors->has('category') ? 'has-error' : '' }}
            <label>Category
                <span 
                  class="text-danger">{{ $errors->first('category') }}
                </span>
            </label>         
            <select class="form-control" name="category">
            <option value="">Select Category</option>
              @foreach ($categorys as $category)
                <option 
                  value="{{$category->id}}"
                  
                >
                @if($category->main_id !=0)
                    &nbsp;&nbsp;&nbsp;
                @endif
                  {{$category->category}}
                </option>
              @endforeach 
            </select>
        </div>
      </div>
        <div class="rows">
            <div 
                class="col-xs-6 {{ $errors->has('public_date') ? 'has-error' : '' }}">  
					<label>Public Date 
                        <span 
                          class="text-danger">{{ $errors->first('public_date') }}
                        </span>
                    </label>
					<input 
    					type="text" 
    					name="public_date"
                        id="datepicker" 
					    class="form-control" 
					    placeholder="PUBLIC DATE"
					>
                    {{ $errors->has('status') ? 'has-error' : '' }}
                    <label>status
                        <span 
                            class="text-danger">{{ $errors->first('status') }}
                        </span>
                    </label>   		   
					<select class="form-control" name="status">
						<option value="public">public</option>
						<option value="Draft">Draft</option>
					</select>
					
        		</div>
        	</div>

            <div class="rows">
            <div 
                class="col-xs-6">  
                    <label>Tags</label><br>
                    @foreach ($tag as $tags)     
                    <input 
                        type="checkbox" 
                        name="tag_id[{{$tags->id}}]"
                        value="{{$tags->id}}"
                    >
                    {{$tags->tag_name}}
                    @endforeach
                </div>
            </div>

        	<div class="rows">
        		<div class="col-xs-12">   
        			{{ $errors->has('sort_text') ? '' : '' }}
              <label>Sort Text 
                  <span 
              class="text-danger">{{ $errors->first('sort_text') }}
            </span>
              </label>
              <textarea class="form-control my-editor" name="sort_text">
                
              </textarea>
        		</div>
        	</div>
        	<div class="rows">
        		<div class="col-xs-12 {{ $errors->has('description') ? 'has-error' : '' }}">   
					<label>Description 
                <span 
              class="text-danger">{{ $errors->first('description') }}
            </span>
          </label>
					<textarea class="form-control my-editor" name="description"></textarea>					
        		</div>
        	</div>
        </div>
    <div class="panel-footer clearfix">
        <div class="pull-right">
			<input 
				type="submit" 
				value="CREATE" 
				class="btn btn-primary"
			>
			<a 
				href="{{'/post'}}" 
				class="btn btn-primary"
			>
				CANCEL
			</a>
		</div>
	</div>
</div>
</form>