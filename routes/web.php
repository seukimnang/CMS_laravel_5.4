<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/login','Auth\LoginController@showLoginForm')->name('login');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/logout/','UsersController@logout');

Route::get('/dashboards','DashboradsController@index');

//route users
Route::get('users','UsersController@index');
Route::get('users/create','UsersController@Create');
Route::post('/users/','UsersController@Store');
Route::get('/users/edit/{id}','UsersController@Edit');
Route::post('/users/update/{id}','UsersController@Update');
Route::get('/users/delete/{id}','UsersController@Destroy');

//route tag
Route::get('/tags/','TagsController@index');
Route::get('tags/create','TagsController@Create');
Route::post('/tags/store','TagsController@Store');
Route::get('/tags/edit/{id}','TagsController@Edit');
Route::post('/tags/update/{id}','TagsController@Update');
Route::get('/tags/delete/{id}','TagsController@Destroy');

//route silder
Route::get('/silders/','SildersController@index');
Route::get('silders/create','SildersController@Create');
Route::post('/silders/store','SildersController@Store');
Route::get('/silders/edit/{id}','SildersController@Edit');
Route::post('/silders/update/{id}','SildersController@Update');
Route::get('/silders/delete/{id}','SildersController@Destroy');

//route category
Route::get('/category/','CategoriesController@index');
Route::get('/category/create','CategoriesController@Create');
Route::post('/category/store','CategoriesController@Store');
Route::get('/category/edit/{id}','CategoriesController@Edit');
Route::post('/category/update/{id}','CategoriesController@Update');
Route::get('/category/delete/{id}','CategoriesController@Destroy');

//route post
Route::get('/post/','PostController@index');
Route::get('/post/create','PostController@Create');
Route::post('/post/store','PostController@Store');
Route::get('/post/edit/{id}','PostController@Edit');
Route::post('/post/update/{id}','PostController@Update');
Route::get('/post/delete/{id}','PostController@Destroy');

//route page
Route::get('/page/','PagesController@index');
Route::get('page/create','PagesController@Create');
Route::post('/page/store','PagesController@Store');
Route::get('/page/edit/{id}','PagesController@Edit');
Route::post('/page/update/{id}','PagesController@Update');
Route::get('/page/delete/{id}','PagesController@Destroy');

//route menu
Route::get('/menu/','MenusController@index');
Route::get('menu/create','MenusController@Create');
Route::post('/menu/store','MenusController@Store');
Route::get('/menu/edit/{id}','MenusController@Edit');
Route::post('/menu/update/{id}','MenusController@Update');
Route::get('/menu/delete/{id}','MenusController@Destroy');

//Sub menu
Route::get('/submenu/','SubmenusController@index');
Route::get('submenu/create','SubmenusController@Create');
Route::post('/submenu/store','SubmenusController@Store');
Route::get('/submenu/edit/{id}','SubmenusController@Edit');
Route::post('/submenu/update/{id}','SubmenusController@Update');
Route::get('/submenu/delete/{id}','SubmenusController@Destroy');

