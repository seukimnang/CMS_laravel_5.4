@extends('layouts.app')
<link href="{{ asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
<script type="text/javascript" src="{{ url('') }}/vendor/tinymce/tinymce.min.js"></script>

<script>
    var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    height : "200" , 
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h3 
        class="page-header" 
        style="color: #428bca;"> 
        Create Page
    </h3>
<form 
    class="form-horizontal" 
    method="POST" 
    action="{{ ('/page/update/'.$page->id) }}">
    {{ csrf_field() }}
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Create Page</h3>
    </div>
        <div class="panel-body">
        	<div class="rows">
        		<div class="col-xs-6 {{ $errors->has('title') ? 'has-error' : '' }}">	    <label>
                        title
                        <span 
                            class="text-danger">{{ $errors->first('title') }}
                        </span>
                    </label>
					<input 
					type="text" 
					name="title"
					value="{{$page->title}}" 
					class="form-control" 
					placeholder="TITLE"
					>
        		</div>
        	</div>
        	<div class="rows">
        		<div 
                    class="col-xs-12 {{ $errors->has('description') ? 
                           'has-error' : '' }}">  
        			<label>
                        description
                        <span 
                            class="text-danger">{{ $errors->first('description') }}
                        </span>
                    </label> 
					<textarea 
						class="form-control my-editor" 
						name="description">	
					{{$page->description}}
					</textarea>
        		</div>
        	</div>
        </div>
    <div class="panel-footer clearfix">
        <div class="pull-right">
			<input 
				type="submit" 
				value="UPDATE" 
				class="btn btn-primary"
			>
			<a 
				href="{{'/page'}}" 
				class="btn btn-primary"
			>
				CANCEL
			</a>
		</div>
	</div>
</div>
</form>