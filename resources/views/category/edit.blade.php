@extends('layouts.app')
<link href="{{ asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h3 class="page-header" style="color: #428bca;"> 
        Update Category
    </h3>
<form action="{{('/category/update/'.$categorys->id)}}" method="POST" class="form-horizontal"  >
                        {{ csrf_field() }}
<div class="panel panel-default">   
    <div class="panel-heading">
        <h3 class="panel-title">Update Category</h3>
    </div>
        <div class="panel-body">
        	<div class="rows">
        		<div class="col-xs-6 {{ $errors->has('category') ? 'has-error' : '' }}">	   	<label>Category Name
						<span 
							class="text-danger">{{ $errors->first('category') }}
						</span>
					</label>
					<input 
						type="text" 
						name="category" 
						class="form-control" 
						value="{{$categorys->category}}" 
						placeholder="CATEGORY NAME"
					>
					<label>Sub Of Category</label>			   
					<select class="form-control" name="main_id">
						<option value="0">Select Sub Of Category</option>
							@foreach ($category as $category)
						<option value="{{$category->id}}"
							{{ $category->id == $categorys->main_id ? 
							'selected="selected"' : '' }}
						>
							{{$category->category}}
						</option>
							@endforeach	
					</select>
					
					

        		</div>
        	</div>
        	<div class="rows">
        		<div class="col-xs-6 {{ $errors->has('description') ? 'has-error' : '' }}">	
        			<label>Descriptoin 
        				<span 
							class="text-danger">{{ $errors->first('description') }}
						</span>
        			</label>
        		<textarea class="form-control" name="description" rows="4">{{$categorys->description}}</textarea>
        		</div>
        	</div>
        </div>
    <div class="panel-footer clearfix">
        <div class="pull-right">
			<input 
				type="submit" 
				value="Update" 
				class="btn btn-primary"
			>
			<a 
				href="{{'/category'}}" 
				class="btn btn-primary"
			>
				CANCEL
			</a>
		</div>
	</div>
</div>
</form>