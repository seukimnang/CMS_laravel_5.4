<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Page;
use Session;
class PagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	$page = Page::where('activate',1)->paginate(10);
    	return view('page.list',compact('page'));
    }
    public function Create()
    {   
    	return view('page.create');
    }

    public function Store(Request $request)
    {
    	$this->validate($request,[
    					'title'=>'required',
    					'description'=>'required',
    					]);

    	$data = new Page;
		$data->title = $request->input('title');  
		$data->description = $request->input('description');        
		$data->save();

        Session::flash('message', 'Create Successfully !');
        return Redirect::to('page');
    }
    public function Edit($id)
    {
        $page = Page::find($id);
        return view('page.edit', ['page' => $page]);

    }
    public function Update(Request $request , $id)
    {
    	$this->validate($request,[
    					'title'=>'required',
    					'description'=>'required',
    					]);
        $data = Page::find($id);
        $data->title = $request->input('title');  
		$data->description = $request->input('description');
        $data->update();
         // redirect
        Session::flash('message', 'Update Successfully !');
        return Redirect::to('page');
    }
    public function Destroy($id)
    {
        //dd($id);
        $page = Page::find($id);
        $page->update(['activate'=> 0]);
        //dd($user);
        Session::flash('message', 'Delete Successfully !');
        return Redirect::to('page');
    }
}
