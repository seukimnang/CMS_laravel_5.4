@extends('layouts.app')
<link href="{{ asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="page-header">
        <a href="{{ ('/menu/create') }}" 
           class="btn btn-primary">
              <i class="fa fa-plus"> 
                add new 
              </i>
        </a>
    </h1>
    @if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
              <tr style="color: #428bca;">
                <th>Menu Name</th>
                <th>Option</th>
              </tr>
            </thead>
              <tbody>
              @foreach ($menu as $menus)
                <tr>
                  <td>{{$menus->menu_name}}</td>
                  <td>
                    <a href="" class="btn btn-default"><i class="fa fa-eye"> View</i></a>
                    <a href="{{('/menu/edit/'.$menus->id)}}" 
                       class="btn btn-default">
                       <i class="fa fa-pencil-square-o">
                       Edit
                       </i>
                    </a>
                    <a href="{{('/menu/delete/'.$menus->id)}}" class="btn btn-danger"><i class="fa fa-trash-o"> Delete</i></a>
                  </td>
                </tr>
              @endforeach
              </tbody>
        </table>
    </div>
</div>
