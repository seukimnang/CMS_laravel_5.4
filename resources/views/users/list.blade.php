@extends('layouts.app')
<link href="{{ asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="page-header">
        <a href="{{ ('/users/create') }}" 
           class="btn btn-primary">
              <i class="fa fa-plus"> 
                add new 
              </i>
        </a>
    </h1>
    @if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
              <tr style="color: #428bca;">
                <th>User Name</th>
                <th>Email</th>
                <th>User Type</th>
                <th width="25%">Option</th>
              </tr>
            </thead>
              <tbody>
              @foreach ($users as $user)
                <tr>
                  <td>{{$user->name}}</td>
                  <td>{{$user->email}}</td>
                  <td>{{$user->user_type}}</td>
                  <td>
                    <a href="" class="btn btn-default"><i class="fa fa-eye"> View</i></a>
                    <a href="{{('/users/edit/'.$user->id)}}" 
                       class="btn btn-default">
                       <i class="fa fa-pencil-square-o">
                       Edit
                       </i>
                    </a>
                    <a href="{{('/users/delete/'.$user->id)}}" class="btn btn-danger"><i class="fa fa-trash-o"> Delete</i></a>
                  </td>
                </tr>
              @endforeach
              </tbody>
        </table>
        {{$users->links()}}
    </div>
</div>
