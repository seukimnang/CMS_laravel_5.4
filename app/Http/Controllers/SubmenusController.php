<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Submenu;
class SubmenusController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	$submenu = Submenu::where('activate',1)->paginate(10);
    	return view('submenu.list',compact('submenu'));
    }
    public function Create()
    {   
    	$category = DB::table('categories')
    				->where('activate',1)->get();
    	$page = DB::table('pages')
    				->where('activate',1)->get();
    	return view('submenu.create',compact('category','page'));
    }

    public function Store(Request $request)
    {
    	$this->validate($request,[
    			'menu_name'=>'required',
    			'order'=>'required',
    			'id_pages'=>'required',
    			'id_category'=>'required',
    			]);
    	$data = new Menu;
		$data->menu_name = $request->input('menu_name');
		$data->order = $request->input('order');
		$data->id_pages = $request->input('id_pages');        
		$data->id_category = $request->input('id_category');
		$data->save();

        Session::flash('message', 'Create Successfully !');
        return Redirect::to('menu');
    }
    public function Edit($id)
    {
        $category = DB::table('categories')
                    ->where('activate',1)->get();
        $page = DB::table('pages')
                    ->where('activate',1)->get();
        $menu = Menu::find($id);
        return view('menu.edit',compact('category','page','menu'));

    }
    public function Update(Request $request , $id)
    {
        $this->validate($request,[
                'menu_name'=>'required',
                'order'=>'required',
                'id_pages'=>'required',
                'id_category'=>'required',
                ]);
        $data = Menu::find($id);
        $data->menu_name = $request->input('menu_name');
        $data->order = $request->input('order');
        $data->id_pages = $request->input('id_pages');        
        $data->id_category = $request->input('id_category'); 
        $data->update();
         // redirect
        Session::flash('message', 'Update Successfully !');
        return Redirect::to('menu');
    }
    public function Destroy($id)
    {
        //dd($id);
        $menu = Menu::find($id);
        $menu->update(['activate'=> 0]);
        //dd($user);
        Session::flash('message', 'Delete Successfully !');
        return Redirect::to('menu');
    }
}
