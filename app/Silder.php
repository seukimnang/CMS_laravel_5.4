<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Silder extends Model
{
    protected $fillable = [
        'title','sort_text','description','img','activate',
    ];
}
