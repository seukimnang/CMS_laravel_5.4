<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Post;
use App\Posts_tag;
use App\User;
use Session;
use Auth;
use Illuminate\Support\Facades\Input;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $user = DB::table('users')
                    ->select('name')
                    ->where('activate',1)->get();
    	$post = Post::where('activate',1)->paginate(10);  
    	return view('post.list',compact('post','user'));
    }

    public function Create()
    {
    	$categorys = DB::table('categories')
    				->select(array('id','category','main_id','order'))
    				->where('activate',1)->orderby('order')->get();
        $tag = DB::table('tags')
                    ->select(array('id','tag_name'))
                    ->where('activate',1)->get();
    	return view('post.create',compact('categorys','tag'));
    }
    public function Store(Request $request)
    {
    	$this->validate($request,[
    			'title'=>'required',
    			'sort_text'=>'required',
    			'public_date'=>'required',
    			'status'=>'required',
    			'category'=>'required',
    			'description'=>'required',
    			]);
    	$data = new Post;
		$data->title = $request->input('title');
		$data->sort_text = $request->input('sort_text');
		$data->public_date = $request->input('public_date');
		$data->status = $request->input('status'); 
        $data->author = Auth::id(); 
		$data->category_id = $request->input('category');  
		$data->description = $request->input('description');
        $data->save();
        //dd($data);
        $id=$data->id;
        //dd($id);
        $tag=Input::get('tag_id');
        //dd($tag);
        if ($tag != "") {
            foreach($tag as $tags_id)
            {
            DB::insert('INSERT INTO posts_tags (id_tag,id_post) VALUES (?,?)', array($tags_id,$id));
            }
        }else{
            
        }
        Session::flash('message', 'Create Successfully !');
        return Redirect::to('post');           
    }

    public function Edit($id)
    {
        $categorys = DB::table('categories')
                    ->select(array('id','category','main_id'))
                    ->where('activate',1)->orderby('order')->get();
        $tag = DB::table('tags')
                    ->select(array('id','tag_name'))
                    ->where('activate',1)->get();
        $post_tag = DB::table('Posts_tags')
                    ->select(array('id_tag'))
                    ->where('activate',1)->where('id_post',$id)->get();
        $posttags = array();
        foreach ($post_tag as $posts_tags) {
            array_push($posttags,$posts_tags->id_tag);
        }
        
        $post = Post::find($id);
        return view('post.edit', 
            compact('categorys','post','tag','posttags'));
    }

    public function Update(Request $request, $id)
    {
        $this->validate($request,[
                'title'=>'required',
                'sort_text'=>'required',
                'public_date'=>'required',
                'status'=>'required',
                'category'=>'required',
                'description'=>'required',
                ]);
        $data = Post::find($id);
        $publicdate=date('Y-m-d h:i a');
        $data->title = $request->input('title');
        $data->sort_text = $request->input('sort_text');
        $data->public_date =$request->input('public_date',$publicdate);
        $data->status = $request->input('status'); 
        $data->update_by = Auth::id();
        $data->category_id = $request->input('category');  
        $data->description = $request->input('description');       
        $idpost=$data->id;
        $tag=Input::get('tag_id');
      
        if ($tag != "") {
            DB::update('UPDATE posts_tags SET activate = 0 where id_post='.$id.'');

            foreach($tag as $tags_id)
            {
            DB::insert('INSERT INTO posts_tags (id_tag,id_post) VALUES (?,?)', array($tags_id,$idpost));
            }
        }else{
            DB::update('UPDATE posts_tags SET activate = 0 where id_post='.$id.'');
        }

        $data->update();
        Session::flash('message', 'Update Successfully !');
        return Redirect::to('post');
    }

    public function Destroy($id)
    {
        //dd($id);
        $data = Post::find($id);
        $data->activate = 0;
        $data->delete_by = Auth::id();
        $data->update();
        // dd($post);

        Session::flash('message', 'Delete Successfully !');
        return Redirect::to('post');
    }
}
