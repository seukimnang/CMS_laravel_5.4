<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
    			'id','title','sort_text','description','status',
    			'author','public_date','category_id','activate',
    			'update_by','delete_by',
    			];
    public $timestamps = false;			
}
