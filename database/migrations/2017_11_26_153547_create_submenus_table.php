<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubmenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submenus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sub_menu');
            $table->integer('order');
            $table->integer('menu_id')->unsigned();
            $table->integer('id_category')->unsigned();
            $table->integer('id_pages')->unsigned();
            $table->foreign('id_category')->references
                            ('id')->on('categories');
            $table->foreign('id_pages')->references
                            ('id')->on('pages');
            $table->foreign('menu_id')->references
                            ('id')->on('menus');
            $table->boolean('activate')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('submenus');
    }
}
