<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboradsController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	$user['admin']="admin page";
    	return view('dashboard.dashboards',$user);
    }
}
