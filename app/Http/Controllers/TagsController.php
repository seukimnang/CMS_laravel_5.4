<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Tag;
use Session;
class TagsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	$tag = Tag::where('activate',1)->paginate(10);
    	return view('tag.list',compact('tag'));
    }
    public function Create()
    {   
    	return view('tag.create');
    }

    public function Store(Request $request)
    {
        $this->validate($request,[
                'tag_name'=>'required',
                ]);
    	$data = new Tag;
		$data->tag_name = $request->input('tag_name');        
		$data->save();
        Session::flash('message', 'Create Successfully !');
        return Redirect::to('tags');
    }
    public function Edit($id)
    {
        $tag = Tag::find($id);
        return view('tag.edit', ['tags' => $tag]);

    }
    public function Update(Request $request , $id)
    {
        $this->validate($request,[
                'tag_name'=>'required',
                ]);
        $data = Tag::find($id);
        $data->tag_name = $request->input('tag_name');     
        $data->update();
         // redirect
        Session::flash('message', 'Update Successfully !');
        return Redirect::to('tags');
    }
    public function Destroy($id)
    {
        //dd($id);
        $tag = Tag::find($id);
        $tag->update(['activate'=> 0]);
        //dd($user);
        Session::flash('message', 'Delete Successfully !');
        return Redirect::to('tags');
    }
}
